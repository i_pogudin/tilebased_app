import pygame
from settings import *
Vector = pygame.math.Vector2


def collide_hit_rect(one, two):
    return one.hit_rect.colliderect(two.rect)


class Player(pygame.sprite.Sprite):

    def __init__(self, game, x, y):
        self.game = game
        self.groups = self.game.all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.image = self.game.player_img
        self.rect = self.image.get_rect()
        self.hit_rect = PLAYER_HIT_RECT
        self.hit_rect.center = self.rect.center
        self.vel = Vector(0, 0)
        self.pos = Vector(x, y) * TILESIZE
        self.rot = 0
        self.rot_speed = 0

    def get_keys(self):
        self.vel = Vector(0, 0)
        self.rot_speed = 0
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] or keys[pygame.K_a]:
            self.rot_speed = PLAYER_ROTATION_SPEED
        if keys[pygame.K_RIGHT] or keys[pygame.K_d]:
            self.rot_speed = -PLAYER_ROTATION_SPEED
        if keys[pygame.K_UP] or keys[pygame.K_w]:
            self.vel = Vector(PLAYER_SPEED, 0).rotate(-self.rot)
        if keys[pygame.K_DOWN] or keys[pygame.K_s]:
            self.vel = Vector(-PLAYER_SPEED / 2, 0).rotate(-self.rot)

        if self.vel.x != 0 and self.vel.y != 0:
            self.vel *= 0.7071

    def collision(self, dir):
        if dir == 'x':
            hits = pygame.sprite.spritecollide(self,
                                               self.game.walls,
                                               False,
                                               collide_hit_rect
                                               )
            if hits:
                # if vx > 0 that means that we are moving right
                if self.vel.x > 0:
                    # make self.x equal to the left-side x of an object
                    self.pos.x = hits[0].rect.left - self.hit_rect.width / 2
                if self.vel.x < 0:
                    self.pos.x = hits[0].rect.right + self.hit_rect.width / 2
                self.vel.x = 0
                self.hit_rect.centerx = self.pos.x
        if dir == 'y':
            hits = pygame.sprite.spritecollide(self,
                                               self.game.walls,
                                               False,
                                               collide_hit_rect
                                               )
            if hits:
                if self.vel.y > 0:
                    self.pos.y = hits[0].rect.top - self.hit_rect.height / 2
                if self.vel.y < 0:
                    self.pos.y = hits[0].rect.bottom + self.hit_rect.height / 2
                self.vel.y = 0
                self.hit_rect.centery = self.pos.y

    def update(self):
        self.get_keys()
        self.rot = (self.rot + self.rot_speed * self.game.dt) % 360
        self.image = pygame.transform.rotate(self.game.player_img, self.rot)
        self.rect = self.image.get_rect()
        self.rect.center = self.pos
        self.pos += self.vel * self.game.dt
        self.hit_rect.centerx = self.pos.x
        self.collision('x')
        self.hit_rect.centery = self.pos.y
        self.collision('y')
        self.rect.center = self.hit_rect.center


class Wall(pygame.sprite.Sprite):

    def __init__(self, game, x, y):
        self.game = game
        self.groups = self.game.all_sprites, self.game.walls
        pygame.sprite.Sprite.__init__(self, self.groups)

        self.image = pygame.Surface((TILESIZE, TILESIZE))
        self.image.fill(BLUE)
        self.rect = self.image.get_rect()
        self.x = x
        self.y = y
        self.rect.x = self.x * TILESIZE
        self.rect.y = self.y * TILESIZE
