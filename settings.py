import pygame
# Screen settings
WIDTH = 1024
HEIGHT = 704
FPS = 60

# Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
DARKGREY = (40, 40, 40)
LIGHTGREY = (180, 180, 180)
YELLOW = (255, 255, 255)

# Tiles
TILESIZE = 64
GRIDWIDTH = WIDTH / TILESIZE
GRIDHEIGHT = HEIGHT / TILESIZE

# Player settings
PLAYER_SPEED = 300
PLAYER_ROTATION_SPEED = 240
PLAYER_HIT_RECT = pygame.Rect(0,0, 35, 35)
