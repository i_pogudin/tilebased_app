import pygame
import sys
import os

from settings import *
from sprites import *
from tile_map import *


class Game:

    def __init__(self):
        pygame.mixer.pre_init(44100, -16, 2, 2048)
        pygame.mixer.init()
        pygame.init()

        self.is_running = True
        self.clock = pygame.time.Clock()
        self.window = pygame.display.set_mode((WIDTH, HEIGHT))
        self.load_data()

    def load_data(self):
        self.game_dir = os.path.dirname(__file__)
        self.img_dir = os.path.join(self.game_dir, 'img')
        self.map = Map(os.path.join(self.game_dir, 'map2.txt'))
        self.player_img = pygame.image.load(os.path.join(self.img_dir,
                                            'manBlue_gun.png')
                                            ).convert_alpha()

    def setup(self):
        self.all_sprites = pygame.sprite.Group()
        self.walls = pygame.sprite.Group()

        self.camera = Camera(self.map.width, self.map.height)

        for row, tiles in enumerate(self.map.data):
            for col, tile in enumerate(tiles):
                if tile == '1':
                    Wall(self, col, row)
                if tile == 'P':
                    self.player = Player(self, col, row)

    def run(self):
        self.playing = True
        while self.playing:
            self.dt = self.clock.tick(FPS) / 1000
            self.events()
            self.update()
            self.draw()

    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.quit()

    def update(self):
        self.all_sprites.update()
        self.camera.update(self.player)

    def draw_grid(self):
        for x in range(0, WIDTH, TILESIZE):
            pygame.draw.line(self.window, LIGHTGREY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pygame.draw.line(self.window, LIGHTGREY, (0, y), (WIDTH, y))

    def draw(self):
        self.window.fill(DARKGREY)
        self.draw_grid()
        # self.all_sprites.draw(self.window)
        for sprite in self.all_sprites:
            self.window.blit(sprite.image, self.camera.apply(sprite))
<<<<<<< HEAD
        # pygame.draw.rect(self.window, WHITE, self.player.hit_rect, 2)
=======
        pygame.draw.rect(self.window, WHITE, self.player.hit_rect, 2)
>>>>>>> 3f80ca8780c6d27164b8c7715143870abfb68a6a
        pygame.display.update()

    def quit(self):
        pygame.quit()
        sys.exit()

    def show_start_screen(self):
        pass

    def show_end_screen(self):
        pass


g = Game()
g.show_start_screen()

while True:
    g.setup()
    g.run()
    g.show_end_screen()
